/*
 * task.js
 *
 * Contains implementation for a "task" "class"
 */

var Task, proto, count;

count = 0;


// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   'use strict';
   var tags, title;

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };

}

/*
 *       Constructors
 */


function makeNewTask() {
   var task;

   count += 1;

   task = Object.create(proto);

   task.title = '';
   task.completedTime = null;

   Object.defineProperty(task, 'id', {
      enumerable: true,
      configurable: false,
      writable: false,
      value: count
   });

   Object.defineProperty(task, 'tags', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: []
   });

   Object.preventExtensions(task);

   return task;
}

function makeTaskFromObject(o) {
   var newTask;

   newTask = Task.new();
   newTask.setTitle(o.title);
   newTask.addTags(o.tags);

   return newTask;
}

function makeTaskFromString(str) {
   var task, o;

   str = str.trimLeft();
   o = processString(str);
   task = Task.fromObject(o);

   return task;
}


/*
 *       Prototype / Instance methods
 */

proto = {
   setTitle: function setTitle(s) {
      this.title = s.trim();

      return this;
   },

   isCompleted: function isCompleted() {
      if (this.completedTime === null) {
         return false;
      }

      return true;
   },

   toggleCompleted: function toggleCompleted() {
      if (this.completedTime !== null) {
         this.completedTime = null;
      } else {
         this.completedTime = new Date();
      }

      return this;
   },

   hasTag: function hasTag(s) {
      if (this.tags.indexOf(s) >= 0) {
         return true;
      }

      return false;
   },

   addTag: function addTag(s) {
      if (this.tags.indexOf(s) >= 0) {
         return this;
      }

      this.tags.push(s);

      return this;
   },

   removeTag: function removeTag(s) {
      var i;

      for (i = 0; i < this.tags.length; i += 1) {
         if (this.tags[i] === s) {
            this.tags.splice(i, 1);
         }
      }

      return this;
   },

   toggleTag: function toggleTag(s) {
      var position;

      if (this.tags.indexOf(s) >= 0) {
         position = this.tags.indexOf(s);
         this.tags.splice(position, 1);

         return this;
      } else {
         this.tags.push(s);
      }

      return this;
   },

   addTags: function addTags(arr) {
      var i;

      for (i = 0; i < arr.length; i += 1) {
         if (this.tags.indexOf(arr[i]) === -1) {
            this.tags.push(arr[i]);
         }
      }

      return this;
   },

   removeTags: function removeTags(arr) {
      var i, position;

      for (i = 0; i < arr.length; i += 1) {
         if (this.tags.indexOf(arr[i]) >= 0) {
            position = this.tags.indexOf(arr[i]);
            this.tags.splice(position, 1);
         }
      }

      return this;
   },

   toggleTags: function toggleTags(arr) {
      var i, position;

      for (i = 0; i < arr.length; i += 1) {
         if (this.tags.indexOf(arr[i]) >= 0) {
            position = this.tags.indexOf(arr[i]);
            this.tags.splice(position, 1);
         } else {
            this.tags.push(arr[i]);
         }

         return this;
      }
   },

   clone: function clone() {
      var task;

      task = Task.new();
      task.title = this.title;
      task.completedTime = this.completedTime;

      this.tags.forEach(function(el) {
         task.tags.push(el);
      });

      return task;
   }
};

// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
