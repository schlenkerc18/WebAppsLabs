/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

// ADD YOUR TESTS HERE
describe('Your code for makeNewTask()', function() {
   var task;

   task = Task.new();
   it('returns an object', function() {
      expect(task).to.be.a('object');
   });
   it('object has properties id, tags, title, and completedTime', function() {
      expect(task).to.have.property('id');
      expect(task).to.have.property('tags');
      expect(task).to.have.property('title');
      expect(task).to.have.property('completedTime');
   });
});

describe('your prototype methods', function() {
   var task;

   beforeEach(function() {
      task = Task.new();
   });

   it('setTitle prototype sets title', function() {
      task.setTitle('task');
      expect(task.title).to.equal('task');
   });

   it('isCompleted prototype returns false if isCompleted is null', function() {
      expect(task.isCompleted()).to.equal(false);
   });

   it('isCompleted prototype returns true if isCompleted is not null', function() {
      task.toggleCompleted();
      expect(task.isCompleted()).to.equal(true);
   });

   it('toggleCompleted prototype can change null to current date', function() {
      task.toggleCompleted();
      expect(task.completedTime).to.not.equal(null);
   });

   it('toggleCompleted prototype can change date to null', function() {
      task.toggleCompleted();
      task.toggleCompleted();
      expect(task.completedTime).to.equal(null);
   });

   it('hasTag prototype retuns false when tag is not in tags', function() {
      expect(task.hasTag('colts')).to.equal(false);
   });

   it('hasTag prototype returns true when tag is in tags', function() {
      task.addTag('colts');
      expect(task.hasTag('colts')).to.equal(true);
   });

   it('addTag prototype can add a tag', function() {
      task.addTag('playoffs');
      expect(task.hasTag('playoffs')).to.equal(true);
   });

   it('removeTag prototype can remove tags', function() {
      task.addTag('colts');
      task.removeTag('colts');
      expect(task.hasTag('colts')).to.equal(false);
   });

   it('toggleTag prototype can remove tag from array', function() {
      task.addTag('colts');
      task.toggleTag('colts');
      expect(task.hasTag('colts')).to.equal(false);
   });

   it('toggleTag prototype can add tag to array', function() {
      task.toggleTag('colts');
      expect(task.hasTag('colts')).to.equal(true);
   });

   it('addTags prototype can add array of tags to tags', function() {
      var arr;

      arr = ['colts', 'playoffs'];
      task.addTags(arr);
      expect(task.hasTag('colts')).to.equal(true);
      expect(task.hasTag('playoffs')).to.equal(true);
   });

   it('removeTags prototype can remove array of tags from tags', function() {
      var arr;

      arr = ['colts', 'playoffs'];
      task.addTags(arr);
      expect(task.hasTag('colts')).to.equal(true);
      expect(task.hasTag('playoffs')).to.equal(true);

      task.removeTags(arr);
      expect(task.hasTag('colts')).to.equal(false);
      expect(task.hasTag('playoffs')).to.equal(false);
   });

   it('toggleTags prototype can remove and add tags from tags', function() {
      var arr1, arr2;

      arr1 = ['colts', 'playoffs', 'championship'];
      arr2 = ['patriots', 'championship', 'never'];

      task.addTags(arr1);
      task.toggleTags(arr2);

      expect(task.hasTag('colts')).to.equal(true);
      expect(task.hasTag('playoffs')).to.equal(true);
      expect(task.hasTag('patriots')).to.equal(true);
      expect(task.hasTag('championship')).to.equal(true);
      expect(task.hasTag('never')).to.equal(false);
   });

   it('clone prototype can clone a task', function() {
      var task2;

      task.addTag('colts');
      task.setTitle('task');

      task2 = task.clone();
      expect(task2.title).to.equal('task');
      expect(task2.hasTag('colts')).to.equal(true);
      expect(task2.id).to.equal(19);
   });
});

describe('your code for makeTaskFromObject()', function() {
   var task, o;

   o = {
      title: 'chores',
      tags: ['cleaning', 'mop', 'sweep', 'takeOutTrash']
   };

   task = Task.fromObject(o);

   it('returns a task from an object', function() {
      expect(task).to.be.a('object');
      expect(task.title).to.equal('chores');
      expect(task.id).to.equal(2);
      expect(task.hasTag('cleaning')).to.equal(true);
      expect(task.hasTag('mop')).to.equal(true);
      expect(task.hasTag('sweep')).to.equal(true);
      expect(task.hasTag('takeOutTrash')).to.equal(true);
      expect(task.completedTime).to.equal(null);
   });
});

describe('your code for makeTaskFromString()', function() {
   var task, str;

   str = ' hi there! #hotTopic';
   task = Task.fromString(str);

   it('returns a task from a string', function() {
      expect(task).to.be.a('object');
      expect(task.title).to.equal('hi there!');
      expect(task.id).to.equal(3);
      expect(task.hasTag('hotTopic')).to.equal(true);
      expect(task.completedTime).to.equal(null);
   });
});
